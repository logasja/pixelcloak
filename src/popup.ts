import '../html/popup.html.src';

import { Option, NavMenu } from './view/popup-views';
import { DirectoryView, StatusView, PinView } from './view/ipfs-views';

const tab_opts: Option[] = [
  {
    id: 'get-files',
    classes: [],
    textContent: 'Files',
    view: new DirectoryView('ipfs-files'),
  },
  {
    id: 'get-pins',
    classes: [],
    textContent: 'Pins',
    view: new PinView('ipfs-pins'),
  },
  {
    id: 'get-status',
    classes: ['is-active'],
    textContent: 'Status',
    view: new StatusView('ipfs-stats'),
  },
];

async function main() {
  const menu = new NavMenu('menuOptions', tab_opts);
  menu.render();
}

window.addEventListener('load', event => {
  main();
});
