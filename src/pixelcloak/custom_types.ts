export module IPFSTypes {
  export type TypedArray =
    | Int8Array
    | Uint8Array
    | Int16Array
    | Uint16Array
    | Int32Array
    | Uint32Array
    | Uint8ClampedArray
    | Float32Array
    | Float64Array;
  export type Bytes = Buffer | ArrayBuffer | TypedArray;
  export type Bloby = Blob | File;
  export type FileContent =
    | Bytes
    | Bloby
    | string
    | Iterable<number>
    | Iterable<Bytes>
    | AsyncIterable<Bytes>;
  export type UnixTime = Date | { secs: number; nsecs?: number } | number[];
  export interface FileStat {
    name: string;
    type: number;
    size: number;
    cid: any;
    mode: number;
    mtime: UnixTime;
    pinned: boolean;
  }
}

export module FormElements {
  export class Input {
    type: string;
  }

  export class Range extends Input {
    min: number;
    max: number;
    step: number;
    constructor(min: number, max: number, step: number) {
      super();
      this.min = min;
      this.max = max;
      this.step = step;
    }
  }

  export class TextArea extends Input {
    maxLength: number;
    required: boolean;
    placeholder: string;
    rows: number;
    constructor(
      maxLength: number,
      required: boolean,
      placeholder: string = '',
      rows: number = 10
    ) {
      super();
      this.maxLength = maxLength;
      this.required = required;
      this.placeholder = placeholder;
      this.rows = rows;
    }
  }

  interface KeyVal {
    key: string;
    value: string | number;
  }

  export class Select extends Input {
    options: Array<KeyVal>;
    def_sel: string;
    multiple: boolean;
    constructor(options: Array<KeyVal>, def_sel: string, multiple: boolean) {
      super();
      this.options = options;
      this.def_sel = def_sel;
      this.multiple = multiple;
    }
  }

  export class Radio extends Select {
    constructor(options: Array<KeyVal>, def_sel: string) {
      super(options, def_sel, false);
    }
  }
}
