import { FileObject } from '../view/ipfs-views';

const IpfsHttpClient = require('ipfs-http-client');
const all = require('it-all');
import { IPFSTypes } from './custom_types';

// Make this a global ipfs client
export class IPFS {
  static client: any = IpfsHttpClient({
    host: 'localhost',
    port: '5001',
    protocol: 'http',
  });

  static async addFile(
    file: FileObject,
    encrypt: boolean = false
  ): Promise<{
    path: string;
    cid: any;
    mode: number;
    mtime: { secs: number; nsecs: number };
    size: number;
  }> {
    const out = await all(
      IPFS.client.add({
        path: file.path + file.name,
        content: file.content,
        mode: file.mode,
        mtime: file.mtime,
      })
    );
    console.log(out[0]);
    return out[0];
  }

  static async lsDirectory(path: string): Promise<Array<IPFSTypes.FileStat>> {
    let out: Array<IPFSTypes.FileStat> = await all(IPFS.client.files.ls(path));
    const cids = out.map(e => e.cid.toString());
    let pinned = await IPFS.lsPins();
    const p_cids = pinned.map((e: any) => e.cid.toString());

    const common_cid = p_cids.filter((value: string) => cids.includes(value));

    for (let i = 0; i < cids.length; i++) {
      if (common_cid.includes(cids[i])) {
        out[i].pinned = true;
      } else {
        out[i].pinned = false;
      }
    }
    console.debug(out);
    return out;
  }

  static async catFile(path: string, publicKey?: string): Promise<Blob> {
    const recieved_dat = [];

    for await (const chunk of IPFS.client.cat(path)) {
      recieved_dat.push(Buffer.from(chunk));
    }
    let buf = Buffer.concat(recieved_dat);
    // console.debug(buf);
    const FileType = await import(
      /* webpackChunkName: "file-type" */ 'file-type/browser'
    );
    let type = await FileType.fromBuffer(buf);

    if (!type) {
      return new Blob([buf]);
    } else {
      return new Blob([buf], { type: type.mime });
    }
  }

  static async genKey(name: string): Promise<any> {
    const out = await IPFS.client.key.gen(name, {
      type: 'rsa',
      size: 2048,
    });

    console.debug(out);

    return out;
  }

  static async getKeys(): Promise<string[]> {
    const out = await IPFS.client.key.list();

    console.debug(out);

    return out;
  }

  static async getSharableKey(name: string, password: string): Promise<String> {
    const out = await IPFS.client.key.export(name, password);

    console.debug(out);

    return out;
  }

  static async addSharedKey(name: string, pem: string, password: string) {
    const out = await IPFS.client.key.import(name, pem, password);

    console.debug(out);

    return out;
  }

  private static async getRawPins(): Promise<any> {
    const recursive = await all(IPFS.client.pin.ls({ type: 'recursive' }));
    const direct = await all(IPFS.client.pin.ls({ type: 'direct' }));

    return recursive.concat(direct);
  }

  private static fileFromStats = (
    item: {
      cumulativeSize: number;
      type: string;
      size: number;
      cid: Object;
      name: string;
    },
    path?: string,
    prefix: string = '/ipfs'
  ) => ({
    size: item.cumulativeSize || item.size || null,
    type:
      item.type === 'dir' || item.type === 'directory'
        ? 1
        : item.type === 'unknown'
        ? 'unknown'
        : 0,
    cid: item.cid,
    name: name || (path ? path.split('/').pop() : item.cid.toString()),
    path: path || `${prefix}/${item.cid.toString()}`,
  });

  private static async stat(hashOrPath: string): Promise<any> {
    const path = hashOrPath.startsWith('/')
      ? hashOrPath
      : `/ipfs/${hashOrPath}`;

    try {
      const stats = await IPFS.client.files.stat(path);
      console.debug(stats);
      return stats;
    } catch (e) {
      // Discard error and mark DAG as 'unknown' to unblock listing other pins.
      // Clicking on 'unknown' entry will open it in Inspector.
      // No information is lost: if there is an error related
      // to specified hashOrPath user will read it in Inspector.
      return {
        path: hashOrPath,
        cid: hashOrPath,
        type: 'unknown',
      };
    }
  }

  static async lsPins() {
    const pins = await IPFS.getRawPins();

    const stats = await all(
      pins.map((item: any) => IPFS.stat(item.cid.toString()))
    );

    return stats.map((item: any) => {
      item = IPFS.fileFromStats(item, undefined, '/pins');
      item.pinned = true;
      return item;
    });
  }

  static async rmPin(cid: string): Promise<any> {
    const out = await IPFS.client.pin.rm(cid);

    console.debug(out);

    return out;
  }

  static async addPin(cid: string): Promise<any> {
    const out = await IPFS.client.pin.add(cid);

    console.debug(out);

    return out;
  }

  static async isCID(str: string): Promise<boolean> {
    //@ts-ignore
    const { isIPFS } = await import(/* webpackChunkName: "ipfs" */ 'ipfs');
    const res: boolean = isIPFS.cid(str);

    console.debug(res);

    return res;
  }
}
