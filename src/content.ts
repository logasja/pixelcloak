import { icon } from './options';
import cheerio from 'cheerio';

/**
 * Port setup
 */
var backgroundPort = browser.runtime.connect({ name: 'port-from-cs' });
// Connection acknowledgement

var buttonsEl: HTMLDivElement | undefined;

// Debug statement
console.debug('Starting');

const config = {
  attributes: true,
  childList: true,
  subtree: true,
};

var processed: Set<HTMLLinkElement> = new Set<HTMLLinkElement>();

const callback = function(
  mutationsList: MutationRecord[],
  observer: MutationObserver
) {
  // console.info("Callback");
  mutationsList = mutationsList.filter(value => value.type === 'childList');

  if (
    !buttonsEl ||
    !buttonsEl.isConnected ||
    !document
      .getElementById('pixelcloak_button')
      ?.textContent?.includes('PixelCloak')
  ) {
    const classlist =
      'cxgpxx05 stjgntxs ni8dbmo4 n1l5q3vz kwzhilbh qypqp5cg lhclo0ds j83agx80 bo76ibdq';
    const query = document.getElementsByClassName(classlist);
    if (query.length > 0) {
      buttonsEl = query.item(0) as HTMLDivElement;
      addStegButton(buttonsEl);
    }
  }

  // Select added elements that are images
  mutationsList.forEach(mutation => {
    const el = mutation.target as HTMLElement;

    const queried = el.querySelectorAll(
      "[href^='https://www.facebook.com/photo/?fbid=']"
    );

    const uploading = el.querySelectorAll('img[alt="nonex.jpg"]');

    if (uploading.length > 0) {
      afterEncode();
    }

    if (queried.length > 0) {
      observer.disconnect();
    }

    queried.forEach((element: HTMLLinkElement) => {
      // Fetch the original image
      if (!processed.has(element)) {
        processed.add(element);
        fetch(element.href, {
          credentials: 'include',
          headers: {
            'User-Agent':
              'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0',
            Accept:
              'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Upgrade-Insecure-Requests': '1',
          },
          method: 'GET',
          mode: 'cors',
        })
          .then(resp => {
            return resp.text();
          })
          .then(text => {
            const $ = cheerio.load(text);
            const out = $(
              'link[data-preloader="adp_CometPhotoRootQueryRelayPreloader_{N}"]'
            ).attr('href');
            if (out) {
              const img = element.querySelector('img');
              img!.src = out;
              // TODO: Here is where I need to modify the class for pixelcloak indicator
            } else {
              console.log(element.href);
              console.log($.html());
            }
          });
      }
    });
  });
  observer.observe(document, config);
};

const observer = new MutationObserver(callback);

observer.observe(document, config);

/*
convert base64 to bolb
*/
function dataURLtoBlob(dataurl: string) {
  var arr = dataurl.split(','),
    mime = arr[0].match(/:(.*?);/)![1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], { type: mime });
}

/*
Add pixelcloak button to Facebook webpage
*/
//@ts-ignore
function addStegButton(el: HTMLDivElement) {
  try {
    let removed = el.removeChild(el.lastChild!);
    console.log('Removed', removed);

    const html = `
      <div class="bp9cbjyn j83agx80 taijpn5t dfwzkoeu ni8dbmo4 stjgntxs">
        <span
          class="pq6dq46d kb5gq1qc pfnyh3mw oi9244e8">
            <i class="sp_bjQWLUSFlbr sx_08bad1" role="img" style="background-image: none;width: 30px;">
              ${icon}
            </i>
        </span>
        <span
          class="d2edcug0 hpfvmrgz qv66sw1b c1et5uql rrkovp55 a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db jq4qci2q a3bd9o3v lrazzd5p m9osqain" dir="auto">
            <span id="pixelcloak_button" class="a8c37x1j ni8dbmo4 stjgntxs l9j0dhe7 ltmttdrg g0qnabr5">
              PixelCloak
            </span>
        </span>
      </div>
      <div
        class="n00je7tq arfg74bv qs9ysxi8 k77z8yql i09qtzwb n7fi1qx3 b5wmifdl hzruof5a pmk7jnqg j9ispegn kr520xx4 c5ndavph art1omkt ot9fgl3s rnr61an3"
        data-visualcompletion="ignore">
      </div>`;
    var stegoButton = document.createElement('div');
    stegoButton.classList.add(
      ...'oajrlxb2 bp9cbjyn g5ia77u1 mtkw9kbi tlpljxtp qensuy8j ppp5ayq2 goun2846 ccm00jje s44p3ltw mk2mc5f4 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 rq0escxv nhd2j8a9 j83agx80 rj1gh0hx btwxx1t3 g5gj957u p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x tgvbjcpo idt9hxom cxgpxx05 dflh9lhu sj5x9vvc scb9dxdr l9j0dhe7 i1ao9s8h esuyzwwr f1sip0of du4w35lb lzcic4wl abiwlrkh p8dawk7l ue3kfks5 pw54ja7n uo3d90p7 l82x9zwi buofh1pr taijpn5t'.split(
        ' '
      )
    );
    stegoButton.setAttribute('role', 'button');
    stegoButton.setAttribute('tabindex', '0');
    stegoButton.style.height = '20px';
    //define the pixelcloak button
    stegoButton.innerHTML = html;

    document.getElementById('steginput')?.remove();
    let steginput = document.createElement('input');
    steginput.id = 'steginput';
    steginput.type = 'file';
    steginput.accept = 'image/*, image/heic, image/heif';
    steginput.multiple = false;
    steginput.hidden = true;

    el.appendChild(steginput);
    el.appendChild(stegoButton);

    stegoButton.onclick = () => {
      // Send it off to the background script
      console.debug('Stegobutton clicked');
      steginput.click();
      steginput.addEventListener('change', () => {
        let file = steginput.files![0];
        console.debug(file);
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function(e) {
          // convert img url to blob
          try {
            let data_bolb = dataURLtoBlob(this.result!.toString());
            backgroundPort.postMessage({
              sender: 'stegbutton',
              bolb: data_bolb,
            });
            steginput.value = '';
          } catch (exception) {
            console.error(exception);
          }
        };
      });
    };
  } catch (error) {
    console.log(error);
  }
}

backgroundPort.onMessage.addListener(function(m: any) {
  console.debug(m);
  // Recieves a blob with new image to insert into form from the backend encoder
  if (m.sender === 'encoder' && m.img) {
    var fb_uploader = buttonsEl!.querySelector('input')!;
    fb_uploader.value = '';
    const change_event = new Event('change', {
      bubbles: true,
      cancelable: false,
    });
    var im_file = new File([m.img], 'nonex.jpg', {
      type: m.img.type,
      lastModified: Date.now(),
    });

    var im_list = new DataTransfer();
    im_list.items.add(im_file);
    var im_files = im_list.files;
    console.log('first id:' + fb_uploader.id);
    fb_uploader.files = im_files;

    setTimeout(function() {
      console.log('second id:' + fb_uploader.id);
      fb_uploader.dispatchEvent(change_event);
    }, 200);
  } else if (m.sender === 'decoder') {
    addDecodedIndicator(m);
  } else if (m.sender === 'validator') {
    const indicator = document.getElementById('uploadID')!;
    const sub = indicator.getElementsByClassName(
      'sub'
    )[0] as HTMLParagraphElement;
    indicator.classList.add(m.success ? 'success' : 'danger');
    sub.textContent = m.success
      ? `Your image's ID was successfully embedded into this image.`
      : `The content ID was not embedded try again.`;
  }
});

// Indicate critical failure if retrieved hash is bad
function afterEncode() {
  // Add indication that image shown is cover image
  let el = document.querySelector('img[alt="nonex.jpg"]') as HTMLImageElement;
  let parel = el!.parentElement;
  if (parel!.children.length > 1) {
    return;
  }
  console.debug('Uploaded Image', el);
  // Check if the embedding is successful
  fetch(el.src)
    .then(response => response.arrayBuffer())
    .then(buff => {
      backgroundPort.postMessage({ sender: 'requestValidate', bolb: buff });
    });
  let indicator = document.createElement('div');
  indicator.id = 'uploadID';
  indicator.classList.add('upload', 'indicator');
  indicator.innerHTML = `<p class="head">Cover image</p><p class="sub">Validating Please Wait...</p>`;
  parel!.insertBefore(indicator, el);
}

// Adds the gotten from pixelcloak indicator to the page
function addDecodedIndicator(m: {
  sender: string;
  original_url: string;
  CID: string;
  mangled: boolean;
}) {
  console.debug(m.original_url);
  // If nothing was retrieved then don't show indicator
  if (m.CID === undefined) {
    return;
  }
  // Get the element retrieved
  let el = document.querySelector(
    `img[src="${m.original_url}"]`
  ) as HTMLElement;
  // console.debug('Retrieved', el);
  // Find the link parent element with limit
  for (let i = 0; i < 6 && el.tagName != 'A' && el != null; i++) {
    console.debug('In loop', el.tagName);
    el = el.parentElement as HTMLElement;
  }
  // console.debug('Found', el);
  let indicators = el.getElementsByClassName('indicator');
  if (indicators.length > 0) {
    indicators[0].remove();
  }
  // Add pixelcloak indicator
  let indicator = document.createElement('div');
  indicator.classList.add('indicator');
  indicator.classList.add(m.mangled ? 'danger' : 'success');
  indicator.innerHTML = m.mangled
    ? `<p class="head">Embedding Failed</p><p class="sub">Delete post, refresh page, and try again</p>`
    : `<p class="head">Retrieved with PixelCloak</p><p class="sub">IPFS CID: ${m.CID}</p>`;
  el.append(indicator);
}

document.onclose = () => {
  observer.disconnect();
};
