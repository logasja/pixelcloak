import { IPFS } from './pixelcloak/service';
import { FileObject } from './view/ipfs-views';
import { getImage, PhotoType } from 'xdne-js';

var blacklist = ['.ico', '.gif', '.png'];
var whitelist = ['.jpg', '.jpeg'];

function handleInstalled(details: any) {
  browser.tabs.create({
    url: './install.html',
  });
}

/*
convert base64 to bolb
*/
// function dataURLtoBlob(dataurl: string) {
//   var arr = dataurl.split(','),
//     mime = arr[0].match(/:(.*?);/)![1],
//     bstr = atob(arr[1]),
//     n = bstr.length,
//     u8arr = new Uint8Array(n);
//   while (n--) {
//     u8arr[n] = bstr.charCodeAt(n);
//   }
//   return new Blob([u8arr], { type: mime });
// }

/*
set up message listener from content script
*/
var contentPort: browser.runtime.Port;

// TODO: Handle case where multiple tabs open, Port array? With Tab ID as index?
function connected(p: browser.runtime.Port) {
  contentPort = p;
  contentPort.onMessage.addListener(function(m: any) {
    //if got chosen image sent from content script
    if (m.sender === 'stegbutton' && m.bolb) {
      //upload image to IPFS system
      // console.log('create data object');
      var data_obj = new FileObject();
      data_obj.content = m.bolb;
      data_obj.mtime = new Date(Date.now());
      data_obj.name = 'pixelcloak_test.jpg';
      data_obj.path = '/';
      IPFS.addFile(data_obj).then(async cid => {
        console.info('New CID: ', cid);
        // get IPFS hash value
        var hash_value = cid.cid.string;

        const iscid = IPFS.isCID(hash_value);
        if (!iscid) {
          throw Error(
            'Output of IPFS client is invalid: \n' + hash_value.toString()
          );
        }

        // // get nonexsitent image
        let cover_image = await getImage(PhotoType.Person);

        const { YASS: YASS, JPEG: JPEG } = await import(
          /* webpackChunkName: "yass" */ 'yass-js'
        );

        // embed hash value into nonexistent image
        let decode = JPEG.decode(cover_image);

        let rawImageData = {
          data: decode.data,
          width: decode.width,
          height: decode.height,
        };

        const embed_obj = {
          str: hash_value,
          key: 'AE0F',
          q: 3,
        };

        let embedded_image = YASS.encode(
          rawImageData,
          undefined,
          embed_obj,
          'facebook'
        );
        var data_bolb = new Blob([Buffer.from(embedded_image.data)], {
          type: 'image/jpeg',
        });

        p.postMessage({ sender: 'encoder', CID: hash_value, img: data_bolb });
      });
    } else if (m.sender === 'requestValidate' && m.bolb) {
      console.debug('Recieved Buffer', m.bolb);
      const keyval = {
        key: 'AE0F',
        q: 3,
      };
      import(/* webpackChunkName: "yass" */ 'yass-js').then(yass => {
        const outCID = yass.YASS.decode(Buffer.from(m.bolb), {
          withKey: keyval,
        }).message;
        IPFS.isCID(outCID ? outCID : '').then(notMangled => {
          console.log('Message to send', {
            sender: 'validator',
            CID: outCID,
            success: notMangled,
          });
          contentPort.postMessage({
            sender: 'validator',
            CID: outCID,
            success: notMangled,
          });
        });
      });
    }
  });
}

// Event to search an image for steganographically encoded information
//  and extract it, replacing the original image with the one decoded.
function handleImage(requestDetails: any) {
  // Promise to avoid blocking the page
  return new Promise(async function(resolve, reject) {
    const { default: bent } = await import(
      /* webpackChunkName: "bent" */ 'bent'
    );

    // Recieve buffer of data from CDN
    const stream = bent('buffer');
    let buffer = await stream(requestDetails.url);

    const keyval = {
      key: 'AE0F',
      q: 3,
    };
    const { YASS: YASS } = await import(
      /* webpackChunkName: "yass" */ 'yass-js'
    );

    let out = YASS.decode(Buffer.from(buffer), { withKey: keyval });

    const reader = new FileReader();
    reader.onload = () => {
      resolve({
        redirectUrl: reader.result,
      });
    };

    // TODO: Detect mangled CID
    // Recieve image data from url as raw data
    let message = {};
    const CID = out.message;
    console.debug(CID);
    if (out.message && (await IPFS.isCID(out.message))) {
      message = {
        sender: 'decoder',
        CID: CID,
        mangled: false,
        original_url: requestDetails.url,
      };
      // TODO: Forward found message to content script
      let blob = await IPFS.catFile(out.message);
      reader.readAsDataURL(blob);
    } else {
      const mangled = CID && (CID.startsWith('Qm') || CID.length == 46);
      message = {
        sender: 'decoder',
        CID: mangled ? CID : undefined,
        mangled: mangled,
        original_url: requestDetails.url,
      };
      // Otherwise load original image
      reader.readAsDataURL(new Blob([buffer], { type: 'image/jpeg' }));
    }
    try {
      contentPort.postMessage(message);
    } catch (error) {
      console.error(error);
      setTimeout(function() {
        contentPort.postMessage(message);
      }, 500);
    }
  });
}

/**
 * Browser webrequest onBeforeRequest to capture incoming images
 */
async function blockRequest(requestDetails: any) {
  // Blacklist icons and gifs from loading
  if (
    blacklist.some(function(v) {
      return requestDetails.url.indexOf(v) >= 0;
    })
  ) {
    return;
    // return { cancel: true };
  }
  // Only process file extensions that have been whitelisted
  else if (
    whitelist.some(function(v) {
      return requestDetails.url.indexOf(v) >= 0;
    })
  ) {
    return handleImage(requestDetails);
  } else {
    console.error(requestDetails.url);
    return;
  }
}

// async function blockUpload(requestDetails: any) {
//   if (requestDetails.method === 'POST') {
//     let { requestBody } = requestDetails;
//     console.debug('Blocking upload request.');
//     console.info(requestDetails);
//     console.error(requestBody.error);
//     console.debug(requestBody.formData);
//     // return {
//     //   cancel: true
//     // }
//   } else {
//     return;
//   }
// }

/**
 * IPFS: Browser before send headers change origin to avoid CORS issues
 */
async function onBeforeSendHeaders(request: any) {
  const apiURL = new URL('http://127.0.0.1:5001');
  const runtimeRoot = browser.runtime.getURL('/');
  const webExtensionOrigin = runtimeRoot ? new URL(runtimeRoot).origin : 'null';

  console.info('Changing Header Origin');
  const { requestHeaders } = request;

  const isWebExtensionOrigin = (origin: string) =>
    origin && new URL(origin).origin === webExtensionOrigin;

  const foundAt = requestHeaders.findIndex(
    (h: { name: string; value: any }) =>
      h.name === 'Origin' && isWebExtensionOrigin(h.value)
  );
  if (foundAt > -1) {
    requestHeaders[foundAt].value = apiURL.origin;
  }

  console.info(requestHeaders);

  return { requestHeaders };
}

// Sets up events and such
async function main() {
  console.info(IPFS.client);
  // Blocking for header origin change
  const onBeforeSendInfoSpec = ['blocking', 'requestHeaders'];
  if (
    (browser.webRequest as any).OnBeforeSendHeadersOptions &&
    'EXTRA_HEADERS' in (browser.webRequest as any).OnBeforeSendHeadersOptions
  ) {
    // Chrome 72+  requires 'extraHeaders' for accessing all headers
    // Note: we need this for code ensuring ipfs-http-client can talk to API without setting CORS
    onBeforeSendInfoSpec.push('extraHeaders');
  }

  console.info('Setting up listener for localhost requests.');
  browser.webRequest.onBeforeSendHeaders.addListener(
    request => onBeforeSendHeaders(request),
    { urls: ['*://localhost/*'] },
    onBeforeSendInfoSpec as any
  );

  // browser.webRequest.onBeforeRequest.addListener(
  //   async request => await blockUpload(request),
  //   {
  //     urls: ['*://upload.facebook.com/*'],
  //   },
  //   ['blocking', 'requestBody']
  // );

  // Blocking for image intercept
  console.info('Setting up listener for cdn requests.');
  browser.webRequest.onBeforeRequest.addListener(
    async details => await blockRequest(details),
    { urls: ['*://*.fbcdn.net/*'], types: ['image'] },
    ['blocking']
  );
}

// Does setup as soon as extension loaded
document.addEventListener('DOMContentLoaded', async () => {
  main();
});

// Listener setups
browser.runtime.onInstalled.addListener(handleInstalled);
browser.runtime.onConnect.addListener(connected);
