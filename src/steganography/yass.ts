import { YASS } from 'yass-js';
import { formParams, displayName, editable } from '../view/forms';
import { FormElements } from '../pixelcloak/custom_types';
import { Steganography } from './stego-base';
import { RawImageData } from 'yass-js/lib/src/types';

export class Yass extends Steganography.Algorithm {
  // Encoding bit layer: 1-7
  @editable
  @displayName('Quality')
  @formParams(new FormElements.Range(0, 100, 1))
  quality: number;

  @editable
  @displayName('Key')
  key: string;

  // Number of times the string is encoded into each image
  @editable
  @displayName('Copies')
  copies: number;

  // Instantiates the class
  constructor(quality: number = 50, key: string, copies?: number) {
    super();
    this.quality = quality;
    this.key = key;
    this.copies = copies || 1;
  }
  decode(dat: Buffer | Uint8Array): Promise<RawImageData<any>> {
    return new Promise<RawImageData<Buffer | Uint8Array>>((resolve, reject) => {
      let out = YASS.decode(dat, {
        withKey: { key: this.key, q: this.copies },
      });
      resolve(out);
    });
  }
  encode(
    dat: RawImageData<Buffer | Uint8Array>,
    message: string
  ): Promise<{
    data: Buffer | Uint8Array;
    width: number;
    height: number;
  }> {
    return new Promise<{
      data: Buffer | Uint8Array;
      width: number;
      height: number;
    }>(resolve => {
      let out = YASS.encode(dat, this.quality, {
        key: this.key,
        q: this.copies,
        str: message,
      });
      resolve(out);
    });
  }
}
