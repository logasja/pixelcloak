// /*
//                                   .       .
//                                  / `.   .' \
//                          .---.  <    > <    >  .---.
//                          |    \  \ - ~ ~ - /  /    |
//                           ~-..-~             ~-..-~
//                       \~~~\.'   stegosploit      `./~~~/
//                        \__/                        \__/
//                          /  .    -.                  \
//                   \~~~\/   {       \       .-~ ~-.    -._ _._
//                    \__/    :        }     {       \     ~{  p'-._
//     .     .   |~~. .'       \       }      \       )      \  ~--'}
//     \\   //   `-..'       _ -\      |      _\      )__.-~~'='''''
//  `-._\\_//__..-'      .-~    |\.    }~~--~~  \=   / \
//   ``~---.._ _ _ . - ~        ) /=  /         /+  /   }
//                             /_/   /         {   } |  |
//                               `~---o.       `~___o.---'

// Image Decoder Javascript function.
// This is the same decoder used in the HTML+Image polyglot
// to trigger the exploit code upon document load.

// by Saumil Shah @therealsaumil
// modifications by Jacob Logas @logasja
// */

// import { Steganography } from './stego-base';
// import { formParams, displayName, editable } from '../view/forms';
// import Jimp from 'jimp';
// import { FormElements } from '../pixelcloak/custom_types';

// export class StegoSploit extends Steganography.Algorithm {
//   // Encoding bit layer: 1-7
//   @editable
//   @displayName('Bit Layer')
//   @formParams(new FormElements.Range(0, 7, 1))
//   bit_layer: number;
//   // Enumerated from RGB to R: 0, G: 1, B: 2, All: 3
//   @editable
//   @displayName('Channel')
//   @formParams(
//     new FormElements.Radio(
//       [
//         {
//           key: 'R',
//           value: 0,
//         },
//         {
//           key: 'G',
//           value: 1,
//         },
//         {
//           key: 'B',
//           value: 2,
//         },
//         {
//           key: 'All',
//           value: 3,
//         },
//       ],
//       'All'
//     )
//   )
//   channel: number;
//   // Pixel grid. 1 = 1x1, 2 = 2x2 and so on
//   @editable
//   @displayName('Grid')
//   grid: number;
//   // Number of times the string is encoded into each image
//   @editable
//   @displayName('Copies')
//   copies: number;

//   // Instantiates the class
//   constructor(
//     bit_layer: number = 1,
//     channel: number = 0,
//     grid: number = 3,
//     copies?: number
//   ) {
//     super();
//     this.bit_layer = bit_layer;
//     this.channel = channel;
//     this.grid = grid;
//     this.copies = copies || 1;
//   }

//   /**
//    * Gets bit n in the pixel array.
//    * @param pix: pixel data of the input image
//    * @param n: the bit to retrieve
//    */
//   getBit(pix: Buffer, n: number): string {
//     var bit;
//     switch (this.channel) {
//       case 0: // red
//       case 1: // green
//       case 2: // blue
//         bit = (pix[n + this.channel] & (1 << this.bit_layer)) >> this.bit_layer;
//         break;
//       default:
//         var rb = (pix[n + 0] & (1 << this.bit_layer)) >> this.bit_layer;
//         var gb = (pix[n + 1] & (1 << this.bit_layer)) >> this.bit_layer;
//         var bb = (pix[n + 2] & (1 << this.bit_layer)) >> this.bit_layer;
//         var mean = (rb + gb + bb) / 3;
//         bit = Math.round(mean);
//     }
//     return String.fromCharCode(bit + 0x30);
//   }

//   /**
//    * Decodes the provided image data using the parameters set earlier
//    * @param img: Image object
//    */
//   async decode(dat: any): Promise<string | void> {
//     return Jimp.read(dat)
//       .then(image => {
//         // The total number of pixels in the image
//         let size = image.getWidth() * image.getHeight();
//         // The maximum possible length for a message based on chosen grid
//         // and copies
//         let max_len = Math.floor(size / (this.copies * Math.pow(this.grid, 2)));

//         /**
//          *
//          * @param stego: This stego object
//          * @param len: The length of the message to be decoded.
//          * @param start_idx: Optional argument for retrieval of string not at beginning
//          */
//         var readBitstream = function(
//           stego: StegoSploit,
//           len: number,
//           start_idx: number = 0
//         ) {
//           let a: never[] | string[] = [];
//           let idx = start_idx;
//           for (var p = 0, j = 0; j < len * 8; j++) {
//             a[p++] = stego.getBit(image.bitmap.data, idx);
//             // Increment by grid
//             idx += 4 * Math.pow(stego.grid, 2);
//           }
//           return a;
//         };

//         // First six encode the length of the hidden message
//         let lens = [];
//         for (let i = 0; i < this.copies; i++) {
//           let result = readBitstream(this, 6, i * max_len * 4);
//           lens.push(
//             parseInt(Steganography.Utils.binaryToString(result.join('')))
//           );
//         }

//         // compare encoded lengths and see if there has been corruption
//         if (
//           lens.every(value => {
//             isNaN(value);
//           })
//         ) {
//           console.error('No message found.');
//           throw Error('No message found.');
//         }

//         let len = 0;
//         // If true, means that all are equal
//         if (
//           !!lens.reduce(function(a, b) {
//             return a === b ? a : NaN;
//           })
//         ) {
//           len = lens[0];
//         } else {
//           len = Steganography.Utils.boyerMoore(lens);
//         }

//         // console.log(len);

//         let message_offset = 48 * (4 * Math.pow(this.grid, 2));
//         let results = [];
//         for (let i = 0; i < this.copies; i++) {
//           // Use that decoded length to get the full message (also fail early if corrupted)
//           let result = readBitstream(
//             this,
//             len,
//             max_len * i * 4 + message_offset
//           );
//           results.push(Steganography.Utils.binaryToString(result.join('')));
//         }

//         var strData = '';
//         // compare string data and vote merge into coherent message
//         if (
//           !!results.reduce(function(a, b) {
//             return a === b ? a : '';
//           })
//         ) {
//           strData = results[0];
//         } else {
//           for (let i = 0; i < len; i++) {
//             let arr = [];
//             for (let j = 0; j < this.copies; j++) {
//               arr.push(results[j].charAt(i));
//             }
//             // console.log(strData);
//             strData += Steganography.Utils.boyerMoore(arr);
//           }
//         }

//         return strData;
//       })
//       .catch(err => {
//         throw err;
//       });
//   }

//   async encode(dat: any, message: string): Promise<any> {
//     return Jimp.read(dat)
//       .then(image => {
//         // Bitstream of the given string
//         let bitstream = Steganography.Utils.stringToBinary(message);

//         // The total number of pixels in the image
//         let size = image.getWidth() * image.getHeight();

//         // The maximum possible length for a message based on chosen grid
//         // and copies
//         let max_len = Math.floor(size / (this.copies * Math.pow(this.grid, 2)));
//         if (bitstream.length > max_len) {
//           console.error('Not enough space in image');
//           throw Error('Not enough space in image');
//         }
//         var idx = 0;
//         for (var j = 0; j < bitstream.length; j++) {
//           // override the least significant bit of RGB pixels
//           // with our data bit depending upon which encoding
//           // channel is chosen.
//           var bit = bitstream.charCodeAt(j) - 0x30; // "0"
//           bit = bit << this.bit_layer;
//           let mask = 0xff - (1 << this.bit_layer);
//           // encode based on the channel selected
//           for (let k = 0; k < this.copies; k++) {
//             let t_idx = max_len * k * 4 + idx;
//             switch (this.channel) {
//               case 0: // red
//               case 1: // green
//               case 2: // blue
//                 image.bitmap.data[t_idx + this.channel] =
//                   (image.bitmap.data[t_idx + this.channel] & mask) | bit;
//                 break;
//               default:
//                 // encode on all channels
//                 image.bitmap.data[t_idx + 0] =
//                   (image.bitmap.data[t_idx + 0] & mask) | bit;
//                 image.bitmap.data[t_idx + 1] =
//                   (image.bitmap.data[t_idx + 1] & mask) | bit;
//                 image.bitmap.data[t_idx + 2] =
//                   (image.bitmap.data[t_idx + 2] & mask) | bit;
//                 break;
//             }
//           }
//           // Increment by grid
//           idx += 4 * Math.pow(this.grid, 2);
//         }
//         return image;
//       })
//       .catch(err => {
//         console.error(err);
//       });
//   }
// }
