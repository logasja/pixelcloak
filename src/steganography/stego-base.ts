import { RawImageData } from 'yass-js/lib/src/types';

export module Steganography {
  export module Utils {
    /**
     * Convert a binary bit stream ot alphanumeric string.
     * @param binary: bit stream
     */
    export function binaryToString(binary: string) {
      var str = '';
      var i = 0;
      for (; i < binary.length; i += 8) {
        var bit = binary.substr(i, 8);
        str += String.fromCharCode(parseInt(bit, 2));
      }
      return str;
    }

    /**
     * Convert a alphanumeric string to a stream of binary bits.
     * @param str: alphanumeric string
     */
    export function stringToBinary(str: string) {
      var l = str.length.toString();
      var returnString =
        '000000'.substring(0, 6 - l.length) + l.toString() + str;
      return returnString.replace(/[^]{1}/g, function(matchedString) {
        var bin = matchedString.charCodeAt(0).toString(2);
        var retString = '00000000'.substring(0, 8 - bin.length) + bin;
        return retString;
      });
    }

    export function boyerMoore(arr: Array<any>) {
      let mf = 1;
      let m = 0;
      let item = arr[0];
      for (let i = 0; i < arr.length; i++) {
        for (let j = i; j < arr.length; j++) {
          if (arr[i] == arr[j]) m++;
          if (mf < m) {
            mf = m;
            item = arr[i];
          }
        }
        m = 0;
      }
      return item;
    }
  }

  export abstract class Algorithm extends Object {
    abstract async decode(dat: any): Promise<RawImageData<any>>;

    abstract async encode(
      dat: any,
      message: string
    ): Promise<{
      data: Buffer | Uint8Array;
      width: number;
      height: number;
    }>;
  }
}
