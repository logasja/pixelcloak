import { UIView } from './views';

export interface Option {
  id: string;
  classes: string[];
  textContent: string;
  view: UIView;
}

export class Tabs extends UIView {
  options: Option[];
  selected: Option;
  constructor(id: string, options: Option[]) {
    super(id);
    this.options = options;
    this.el.classList.add('tabs');
  }

  static onClick(this: any, obj: Tabs) {
    const id = this.id;
    let choose = obj.options.find(option => option.id == id)!;

    if (choose == obj.selected) {
      return;
    }

    // Clear panel
    obj.clear();
    // Update selection
    obj.selected = choose;
    // Render the newly chosen
    obj.selected.view.render();
    // Update Tab UI
    this.classList.add('is-active'); //@tsignore
  }

  async render() {
    // Get action menu in which we insert
    this.options.forEach(option => {
      // Set up basic structure for list
      let el = document.createElement('li');
      let iel = document.createElement('a');
      el.appendChild(iel);

      iel.innerHTML = option.textContent;
      el.id = option.id;
      el.onclick = Tabs.onClick.bind(el, this);
      if (option.classes.length > 0) {
        option.classes.forEach(cls => {
          el.classList.add(cls);
          if (cls === 'is-active') {
            this.selected = option;
          }
        });
      }
      this.content!.appendChild(el);
    });
    this.selected.view.render();
  }
  clear() {
    var nav_children = this.content!.children;
    for (var i = 0; i < nav_children.length; i++) {
      nav_children[i].classList.remove('is-active');
      this.options[i].view.clear();
    }
  }
}

export class NavMenu extends UIView {
  options: Option[];
  selected: Option;
  brand: HTMLDivElement;

  constructor(id: string, options: Option[]) {
    super(id);
    this.options = options;
    this.registerHamburgerButton();
  }

  static onClick(this: any, obj: Tabs) {
    const id = this.id;
    let choose = obj.options.find(option => option.id == id)!;

    if (choose == obj.selected) {
      return;
    }

    // Clear panel
    obj.clear();
    // Update selection
    obj.selected = choose;
    // Render the newly chosen
    obj.selected.view.render();
    // Update Tab UI
    this.classList.add('is-active');
  }

  private registerHamburgerButton() {
    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(
      document.querySelectorAll('.navbar-burger'),
      0
    );

    // Add a click event on each of them
    $navbarBurgers.forEach((el: HTMLElement) => {
      el.addEventListener('click', () => {
        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        console.log(target);
        const $target = document.getElementById(target!);
        console.log($target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target!.classList.toggle('is-active');
      });
    });
  }

  async render() {
    // Get action menu in which we insert
    this.options.forEach(option => {
      // Set up basic structure for list
      let el = document.createElement('a');
      el.classList.add('navbar-item');

      el.innerHTML = option.textContent;
      el.id = option.id;
      el.onclick = NavMenu.onClick.bind(el, this);
      if (option.classes.length > 0) {
        option.classes.forEach(cls => {
          el.classList.add(cls);
          if (cls === 'is-active') {
            this.selected = option;
          }
        });
      }
      this.content!.appendChild(el);
    });
    this.selected.view.render();
  }
  clear() {
    var nav_children = this.content!.children;
    for (var i = 0; i < nav_children.length; i++) {
      nav_children[i].classList.remove('is-active');
      this.options[i].view.clear();
    }
  }
}
