import { IPFS } from '../pixelcloak/service';
import { DynamicView } from '../view/views';

import { Form, editable, displayName } from './forms';
import { IPFSTypes } from '../pixelcloak/custom_types';

export class FileObject {
  // The path you want to the file to be accessible at from the root CID _after_ it has been added
  @editable
  @displayName('Path')
  path: string;

  // filename
  @editable
  @displayName('File Name')
  name: string;

  // The contents of the file (see below for definition)
  content: IPFSTypes.FileContent;

  // File mode to store the entry with (see https://en.wikipedia.org/wiki/File_system_permissions#Numeric_notation)
  @editable
  @displayName('Permissions')
  mode: number | string;

  // The modification time of the entry (see below for definition)
  mtime: IPFSTypes.UnixTime;

  constructor() {
    this.path = '/';
    this.mode = '600';
    this.name = '';
  }
}

// Form for uploading items to IPFS
export class UploadForm extends Form {
  async submitAction(): Promise<void> {
    IPFS.addFile(this.obj);
  }

  constructor(id: string, obj: FileObject) {
    super(id, obj);
  }

  async render(): Promise<void> {
    this.el.hidden = true;

    let properties: string[] =
      Reflect.getMetadata('editableProperties', this.obj) || [];

    const form = this.generateFormFromProps(properties);
    this.content!.appendChild(form);

    let out = this.generateFileEl();

    form.appendChild(out);

    // Create submit button
    let submit_btn = document.createElement('a');
    submit_btn.classList.add('button');
    submit_btn.classList.add('is-success');
    submit_btn.innerHTML = 'Upload';
    submit_btn.addEventListener('click', ev => {
      ev.preventDefault();
      this.submitAction();
    });

    this.content!.appendChild(submit_btn);

    this.el.hidden = false;
  }

  protected generateFileEl(): HTMLDivElement {
    const div = document.createElement('div');
    div.classList.add('file');
    div.classList.add('has-name');
    div.classList.add('is-boxed');

    div.innerHTML =
      ' \
      <label class="file-label"> \
        <input class="file-input" type="file" name="resume"> \
        <span class="file-cta"> \
          <span class="file-icon"> \
            <i class="fas fa-upload"></i> \
          </span> \
          <span class="file-label"> \
            Choose a file… \
          </span> \
          <span class="file-name">File name</span> \
        </span>\
      </label>';

    const input = div.querySelector('input')!;
    const filename_el = div.getElementsByClassName('file-name')[0];

    input.addEventListener('change', async e => {
      if (input.files && input.files.length == 1) {
        this.obj.mtime = new Date(input.files[0].lastModified);
        this.obj.content = input.files[0];
        this.obj.name = input.files[0].name;
        filename_el.innerHTML = input.files[0].name;
      }
    });

    return div;
  }
}

export async function FileViewFactory(
  id: string,
  fileName: string,
  cid: string
): Promise<IPFSFileView> {
  let blob = await IPFS.catFile(cid);

  if (!blob.type) {
    throw Error(
      'View for that filetype not implemented yet! Open an issue here'
    );
  } else if (blob.type.includes('image')) {
    let out = new IPFSPhotoView(id, cid, fileName, blob);
    return out;
  } else {
    throw Error(
      'View for that filetype not implemented yet! Open an issue here'
    );
  }
}

abstract class IPFSFileView extends DynamicView {
  private cid: string;
  protected data: Blob;
  protected name: string;

  constructor(id: string, cid: string, name?: string, data?: Blob) {
    super(id);
    this.cid = cid;
    if (name) {
      this.name = name;
    }
    if (data) {
      this.data = data;
    }
  }

  abstract async render(): Promise<void>;

  async refresh_data(): Promise<void> {
    this.data = await IPFS.catFile(this.cid);
  }
}

export class IPFSPhotoView extends IPFSFileView {
  async render(): Promise<void> {
    if (!this.data) {
      await this.refresh_data();
    }
    let urlCreator = window.URL;
    let imageUrl = urlCreator.createObjectURL(this.data);
    let img_el = document.createElement('img');
    img_el.src = imageUrl;
    img_el.classList.add('image');

    let title_el = document.createElement('h2');
    title_el.classList.add('title-is-2');
    title_el.innerHTML = this.name;

    this.content!.appendChild(img_el);
    this.content!.appendChild(title_el);

    this.el.hidden = false;
  }
}

export class StatusView extends DynamicView {
  async render() {
    await this.refresh_data();
    this.el.hidden = false;
    for (var p in this.data) {
      let el = document.getElementById(this.id + '-' + p)!;
      el.innerHTML = this.data[p];
    }
  }

  async refresh_data() {
    this.data = await IPFS.client.id();
  }
}

export class DirectoryView extends DynamicView {
  dir: string;
  history: string[];

  constructor(id: string, dir: string = '') {
    super(id);
    if (dir === '' || dir === '/') {
      this.history = ['', ''];
    } else {
      this.history = dir.split('/');
    }
    this.dir = dir;
  }

  // For IPFS traversal
  // TODO: In future maybe get rid of go back element for cd .. instead
  //   using <a> tags in the header element
  static onClickDir(this: HTMLElement, obj: DirectoryView, file: any) {
    // Add directory to history
    obj.history.push(file.name);
    // Create new dir for fetching files
    console.log(obj.history);
    // Refresh the view
    obj.render();
  }

  // When a file is clicked, open it in the sidebar or modal
  // TODO: Implement sidebar capability check
  static onClickFile(this: HTMLElement, obj: any) {
    console.debug(obj);
    this.classList.add('is-active');
    FileViewFactory(this.id, '', this.id).then(file => {
      file.render();
    });
    // browser.sidebarAction.open().then(async () => {
    //   let response = await browser.runtime.sendMessage({
    //     cid: obj.cid.toString(),
    //     fileName: obj.name,
    //   });
    //   console.log(response);
    // });
  }

  async refresh_data() {
    let head = document.getElementById(this.id + '-header')!;
    this.dir = this.history.join('/');
    head.innerHTML = this.dir;
    console.log(this.dir);
    this.data = await IPFS.lsDirectory(this.dir);
  }

  async render() {
    this.clear();

    // TODO: Put spinning loader

    // Grab files in current directory
    await this.refresh_data();

    // Unhide the section of the popup
    this.content!.hidden = true;

    // Add the back button for directory navigation
    if (this.history.length > 2) {
      const b_el = document.createElement('a');
      b_el.innerHTML =
        ' \
        <a class="panel-block"> \
          <span class="panel-icon"> \
            <i class="fas fa-chevron-up" aria-hidden="true"></i> \
          </span> \
        </a>';
      // Define what should happen when the "go back" element is clicked
      b_el.onclick = () => {
        // Pop the most recent element
        this.history.pop()!;
        this.dir = this.history.join('/');
        this.render();
      };
      this.content!.appendChild(b_el);
    }

    // Iterate over the files retrieved from IPFS in the directory
    for (const file of this.data) {
      // Create the container
      let el = document.createElement('a');
      el.id = file.cid.toString();
      el.classList.add('panel-block');

      // Create the elements that go into the container
      let ico_el = document.createElement('span');
      ico_el.classList.add('panel-icon');
      let name_el = document.createElement('p');
      let pin_el = document.createElement('span');
      pin_el.classList.add('panel-icon');

      // Add to original container
      el.appendChild(ico_el);
      el.appendChild(name_el);
      el.appendChild(pin_el);

      // Determine what happens when clicked and the visual used
      //  differs between directory and file
      let ico_class;
      let ico_pin;
      if (file.type == 1) {
        ico_class = 'fa-folder';
        name_el.onclick = DirectoryView.onClickDir.bind(el, this, file);
        ico_el.onclick = DirectoryView.onClickFile.bind(el, file);
      } else {
        ico_class = 'fa-file';
        name_el.onclick = DirectoryView.onClickFile.bind(el, file);
        ico_el.onclick = DirectoryView.onClickFile.bind(el, file);

        // hidden preview element for showing the file
        let preview_el = document.createElement('div');
        preview_el.id = file.cid.toString() + '-content';
        preview_el.classList.add('container');
        preview_el.style.height = '200px';
        preview_el.style.width = '100%';
        preview_el.hidden = true;
        el.appendChild(preview_el);
      }

      // Show indicator that file is pinned
      if (file.pinned) {
        ico_pin = 'has-text-success';
      }

      // Create icon element
      ico_el.innerHTML = `<i class="fas ${ico_class}" aria-hidden="true"></i>`;

      // Show name
      name_el.textContent = file.name;

      // Create pin icon
      pin_el.innerHTML = `<i class="fas fa-map-pin ${ico_pin}" aria-hidden="true"></i>`;

      // define what happens when pin icon is clicked
      pin_el.addEventListener('click', ev => {
        if (file.pinned) {
          IPFS.rmPin(file.cid);
          pin_el.querySelector('i')!.classList.remove('has-text-success');
        } else {
          IPFS.addPin(file.cid);
          pin_el.querySelector('i')!.classList.add('has-text-success');
        }
      });

      this.content!.appendChild(el);
    }
    // Show updated view
    this.el.hidden = false;
    this.content!.hidden = false;
  }
}

export class PinView extends DirectoryView {
  async refresh_data() {
    let head = document.getElementById(this.id + '-header')!;
    head.innerHTML = this.dir;
    this.data = await IPFS.lsPins();
  }
}
