import { UIView } from './views';

export class InstallView extends UIView {
  private os: string;

  constructor(id: string) {
    super(id);
    this.os = InstallView.detectOS();
    this.el.innerHTML = `
      <div class="columns">
        <div class="content column mx-1 is-2 box">
          <h2 class="title is-3">Install IPFS Node</h2>
          <p>
            We have detected you do not have an IPFS node running.
            PixelCloak uses IPFS in the backend to deliver images.
            IPFS is a decentralized way for content to be delivered on the web.
          </p>
        </div>
      </div>`;
    this.content = this.el.getElementsByClassName('columns')[0] as HTMLElement;
  }

  private static detectOS(): string {
    let os = '';
    if (navigator.appVersion.indexOf('Win') != -1) os = 'WIN';
    else if (navigator.appVersion.indexOf('Mac') != -1) os = 'MAC';
    else if (navigator.appVersion.indexOf('X11') != -1) os = 'UNIX';
    else if (navigator.appVersion.indexOf('Linux') != -1) os = 'LINUX';

    return os;
  }

  private async retrieveReleases(): Promise<{
    [key: string]: any;
    [key: number]: any;
  }> {
    const { default: bent } = await import(
      /* webpackChunkName: "bent" */ 'bent'
    );
    const stream = bent('json');
    let releases: {
      [key: string]: any;
      [key: number]: any;
    } = await stream(
      'https://api.github.com/repos/ipfs-shipyard/ipfs-desktop/releases/latest'
    );
    let pref = document.createElement('div');
    let inst: {
      name: string;
      command: string;
    };
    if (this.os == 'WIN') {
      inst = {
        name: 'Chocolatey',
        command: 'choco install ipfs-desktop',
      };
      releases = releases['assets'].filter((item: any) =>
        item.name.endsWith('.exe')
      );
    } else if (this.os == 'MAC') {
      inst = {
        name: 'Homebrew',
        command: 'brew cask install ipfs',
      };
      releases = releases['assets'].filter((item: any) =>
        item.name.endsWith('.dmg')
      );
    } else {
      inst = {
        name: 'snap',
        command: 'snap install ifps-desktop',
      };
      releases = releases['assets'].filter((item: any) =>
        item.name.contains('linux')
      );
    }
    pref.classList.add('column', 'mx-2', 'box');
    pref.innerHTML = `
      <h1 class="title is-4">Package Manager</h1>
      <b>${inst.name}</b><br/><code>${inst.command}</code>
    `;
    this.content!.appendChild(pref);
    return releases;
  }

  async render(): Promise<void> {
    let release = await this.retrieveReleases();

    let dwn_btns = document.createElement('div');
    dwn_btns.innerHTML = `
      <h1 class="title is-4">Manual Install</h1>`;
    dwn_btns.classList.add('column', 'box', 'mx-1', 'mb-5');

    console.debug(release);
    release.forEach((item: any) => {
      let button = document.createElement('a');
      button.classList.add('button', 'is-primary', 'is-fullwidth');
      button.style.minHeight = '100px';
      button.href = item.browser_download_url;
      button.innerHTML = `<div><div><b>Download</b></div><div><small>${item.name}</small></div></div>`;
      dwn_btns.appendChild(button);
    });
    this.content!.appendChild(dwn_btns);
  }
}
