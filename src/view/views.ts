// Base class for a UI element in this context
export abstract class UIView {
  readonly id: string;
  readonly el: HTMLElement;
  content: HTMLElement | null;
  constructor(id: string) {
    this.id = id;
    this.el = document.getElementById(id)!;
    this.content = document.getElementById(id + '-content')!;
  }

  // Clear all DOM elements under both the main element and the content element
  clear() {
    this.el.hidden = true;
    if (this.content !== null) {
      this.content.hidden = true;
      while (this.content.hasChildNodes()) {
        this.content.removeChild(this.content.lastChild!);
      }
    }
  }

  // Renders the DOM elements required
  abstract async render(): Promise<void>;
}

// Base class for a dynamic view element with changing data
export abstract class DynamicView extends UIView {
  protected data: any;

  abstract async render(): Promise<void>;
  abstract async refresh_data(): Promise<void>;
}
