import 'reflect-metadata';
import { UIView } from './views';
import { Steganography } from '../steganography/stego-base';
import { FormElements } from '../pixelcloak/custom_types';

// Decorators for form generation
// Marks a property as editable by a form
export function editable(target: any, propertyKey: string) {
  let properties: string[] =
    Reflect.getMetadata('editableProperties', target) || [];
  if (properties.indexOf(propertyKey) < 0) {
    properties.push(propertyKey);
  }

  Reflect.defineMetadata('editableProperties', properties, target);
}

// Allows for range parameters to be set on control.
export function formParams(params: FormElements.Input) {
  params.type = params.constructor.name;
  return function(target: any, propertyKey: string) {
    Reflect.defineMetadata('params', params, target, propertyKey);
  };
}

export function displayName(name: string) {
  return function(target: any, propertyKey: string) {
    Reflect.defineMetadata('displayName', name, target, propertyKey);
  };
}

export abstract class Form extends UIView {
  protected obj: any;

  constructor(id: string, obj: any) {
    super(id);
    this.obj = obj;
  }

  getObject(): any {
    return this.obj;
  }

  protected generateFormFromProps(properties: string[]): HTMLFormElement {
    const form = document.createElement('form');
    for (let property of properties) {
      const dataType =
        Reflect.getMetadata('design:type', this.obj, property) || property;
      const displayName =
        Reflect.getMetadata('displayName', this.obj, property) || property;
      const params = Reflect.getMetadata(
        'params',
        this.obj,
        property
      ) as FormElements.Input;

      // Create containing div
      const div = document.createElement('div');
      div.classList.add('field');

      // Create label
      const label = document.createElement('label');
      label.classList.add('label');
      label.textContent = displayName;
      label.htmlFor = property;
      div.appendChild(label);

      // Handle case of just input box
      if (!params) {
        // Create input element
        const input = document.createElement('input');
        input.classList.add('input');
        input.id = this.id + '-' + property;
        input.value = this.obj[property].toString();
        if (dataType === String) {
          input.type = 'text';
          input.addEventListener(
            'input',
            e => (this.obj[property] = input.value)
          );
        } else if (dataType === Date) {
          input.type = 'date';
          input.addEventListener(
            'input',
            e => (this.obj[property] = input.valueAsDate)
          );
        } else if (dataType === Number) {
          input.type = 'number';
          input.addEventListener(
            'input',
            e => (this.obj[property] = input.valueAsNumber)
          );
        } else if (dataType === Boolean) {
          input.type = 'checkbox';
          input.addEventListener(
            'input',
            e => (this.obj[property] = input.checked)
          );
        }
        div.appendChild(input);
      }
      // Create element for range input (slider)
      else if (params.type == 'Range') {
        // Helper text used to display selected value
        const out = document.createElement('p');
        out.classList.add('help');
        out.innerHTML = this.obj[property];
        div.appendChild(out);

        const input = document.createElement('input');
        input.classList.add('input');
        input.id = this.id + '-' + property;
        input.value = this.obj[property].toString();
        input.type = 'range';
        input.classList.add('slider');
        input.min = (params as FormElements.Range).min.toString();
        input.max = (params as FormElements.Range).max.toString();
        input.step = (params as FormElements.Range).step.toString();
        input.addEventListener('input', e => {
          this.obj[property] = input.value;
          out.innerHTML = input.value;
        });
        div.appendChild(input);
      }
      // Radio element
      else if (params.type == 'Radio') {
        div.classList.replace('field', 'control');

        (params as FormElements.Radio).options.forEach(element => {
          const lbl = document.createElement('label');
          lbl.classList.add('radio');
          lbl.innerHTML = element.key;

          const input = document.createElement('input');
          input.type = 'radio';
          input.value = element.value.toString();
          input.name = this.id + '-' + property;
          input.checked = element.key == (params as FormElements.Radio).def_sel;
          input.addEventListener(
            'input',
            e => (this.obj[property] = input.value)
          );

          lbl.appendChild(input);
          div.appendChild(lbl);
        });
      }
      // Textarea element
      else if (params.type == 'TextArea') {
        const input = document.createElement('textarea');
        input.classList.add('textarea');
        input.placeholder = (params as FormElements.TextArea).placeholder;
        input.rows = (params as FormElements.TextArea).rows;
        input.addEventListener('focusout', e => {
          this.obj[property] = input.value;
          console.log(input.value);
        });

        div.appendChild(input);
      }
      // TODO: SELECT class handler

      form.appendChild(div);
    }
    return form;
  }

  async render(): Promise<void> {
    this.el.hidden = true;

    let properties: string[] =
      Reflect.getMetadata('editableProperties', this.obj) || [];

    const form = this.generateFormFromProps(properties);
    this.content!.appendChild(form);

    this.el.hidden = false;
  }

  abstract async submitAction(): Promise<void>;
}

// Subclass specifically for steganography algorithms and their changable params
export class StegoForm extends Form {
  async submitAction(): Promise<any> {
    throw new Error('Method not implemented.');
  }
  constructor(id: string, obj: Steganography.Algorithm) {
    super(id, obj);
  }

  async render(): Promise<void> {
    this.el.hidden = true;

    let properties: string[] =
      Reflect.getMetadata('editableProperties', this.obj) || [];

    const form = this.generateFormFromProps(properties);
    this.content!.appendChild(form);

    // TODO: Insert form elements for processing/decoding

    this.el.hidden = false;
  }
}
