import '../html/sidebar.html.src';
import { FileViewFactory, UploadForm, FileObject } from './view/ipfs-views';

async function main() {
  browser.runtime.onMessage.addListener(
    (request: any, sender: any, sendResponse: any) => {
      console.log(sender);
      console.log(request);
      sendResponse({ response: 'Recieved' });
      FileViewFactory('ipfs-file', request.fileName, request.cid).then(file => {
        file.render();
      });
    }
  );
  const uform = new UploadForm('ipfs-add', new FileObject());
  uform.render();
}

window.addEventListener('load', event => {
  main();
});
