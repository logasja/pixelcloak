import '../html/install.html.src';

import { InstallView } from './view/install-views';

async function main() {
  const inview = new InstallView('install');
  inview.render();
}

window.addEventListener('load', event => {
  main();
});
