const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WebExtWebpackPlugin = require('web-ext-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Visualizer = require('webpack-visualizer-plugin');

// Look for a --firefox <path> argument
const firefoxIndex = process.argv.indexOf('--firefox');
const firefox =
  firefoxIndex !== -1 && firefoxIndex < process.argv.length - 1
    ? process.argv[firefoxIndex + 1]
    : undefined;

// Likewise for firefoxProfile
const firefoxProfileIndex = process.argv.indexOf('--firefoxProfile');
const firefoxProfile =
  firefoxProfileIndex !== -1 && firefoxProfileIndex < process.argv.length - 1
    ? process.argv[firefoxProfileIndex + 1]
    : undefined;

const isDevelopment = process.env.NODE_ENV === 'development'

const commonConfig = {
  // No need for uglification etc.
  mode: process.env.NODE_ENV,
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader'
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: isDevelopment
            }
          }
        ]
      },
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: [/node_modules/,]
      },
      { enforce: 'pre', test: /\.js$/, loader: 'source-map-loader',
      exclude:[/node_modules/,]
     },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
};

const commonExtConfig = {
  ...commonConfig,
  entry: {
    'content': './src/content.ts',
    'background': './src/background.ts',
    'sidebar': './src/sidebar.ts',
    'install': './src/install.ts',
    'popup': './src/popup.ts',
    'facebook': './scss/facebook.scss',
    'mystyles': './scss/mystyles.scss'
  },
};

const getPreprocessorConfig = (...features) => ({
  test: /\.src$/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: '[name]',
      },
    },
    {
      loader:
        'webpack-preprocessor?' +
        features.map(feature => `definitions[]=${feature}`).join(','),
    },
  ],
});

const extendArray = (array, ...newElems) => {
  const result = array.slice();
  result.push(...newElems);
  return result;
};

const firefoxConfig = {
  ...commonExtConfig,
  module: {
    ...commonExtConfig.module,
    rules: extendArray(
      commonExtConfig.module.rules,
      getPreprocessorConfig(
        'supports_svg_icons',
        'supports_browser_style',
        'supports_applications_field'
      )
    ),
  },
  output: {
    path: path.resolve(__dirname, 'dist-firefox'),
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
  },
  plugins: [
    // new Visualizer({
    //   filename: '../statistics.html'
    // }),
    new CopyWebpackPlugin([
      'images/*',
      '_locales/**/*',
      { from: "firefox-manifest.json", to: "manifest.json"}
    ]),
    new WebExtWebpackPlugin({
      firefox,
      firefoxProfile,
      browserConsole: true,
      startUrl: ['tests/playground.html'],
      sourceDir: path.resolve(__dirname, 'dist-firefox'),
    }),
    new MiniCssExtractPlugin({
      filename: isDevelopment ? "css/[name].css" : "css/[name].css",
      chunkFilename: isDevelopment ? "css/[id].css" : "css/[id].[hash].css"
    }),
  ],
};

const chromeConfig = {
  ...commonExtConfig,
  module: {
    ...commonExtConfig.module,
    rules: extendArray(
      commonExtConfig.module.rules,
      getPreprocessorConfig('use_polyfill')
    ),
  },
  output: {
    path: path.resolve(__dirname, 'dist-chrome'),
    filename: '[name].js',
  },
  plugins: [
    new CopyWebpackPlugin([
      'images/*',
      'data/*',
      '_locales/**/*',
      { from: "chrome-manifest.json", to: "manifest.json" },
      { from: 'node_modules/webextension-polyfill/dist/browser-polyfill.js' },
    ]),
    new MiniCssExtractPlugin({
      filename: isDevelopment ? "css/[name].css" : "css/[name].css",
      chunkFilename: isDevelopment ? "css/[id].css" : "css/[id].[hash].css"
    }),
  ],
};

module.exports = (env, argv) => {
  let configs = [];
  // let configs = [commonConfig];
  if (env && env.target === 'chrome') {
    configs.push({ ...chromeConfig, name: 'extension' });
  } else {
    configs.push({ ...firefoxConfig, name: 'extension' });
  }

  return configs;
};
