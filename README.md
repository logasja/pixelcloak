# Installation

## Dependencies
Node.js LTS: https://nodejs.org/en/download/

Firefox Developer Edition: https://www.mozilla.org/en-US/firefox/developer/

## Pre-commit
We have a linter + prettier for commits in the repo using husky. If you are getting commit failed messages, it is likely because of a style or definition error.

## Building
Building the extension should be straightforward. When done editing files just run:
```bash
npm run build
```
which will package and output the extension into *dist-firefox*.

# Running
Now open Firefox Developer Edition and go to ☰ > Add-ons. Find the :gear: > 'Debug Add-ons'. On this page select 'Load Temporary Add-on...' and in the 'dist-firefox' folder select manifest.json. This will load the extension and show the sidebar view.

# Debugging
To debug the application make liberal use of console.log() and under the 'Debug Add-ons' page in Firefox select 'Inspect' under the PixelCloak extension element.

# Contributions
When contributing to the repo, be sure to use branches and pull requests. This prevents issues arising related to conflicting commits. Be sure to be descriptive in your comments. Try to avoid ending up on this site http://www.commitlogsfromlastnight.com/.